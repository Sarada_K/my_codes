%Simulation of Kuramoto for 2 oscillators

%prompt = 'Enter the number of numbers to be compared';

%input(prompt);
function[Index] = Max_sort3_trial(A,n)

% display('length');
%display(A);
% display(n);

no = n; %randi([10,20],1,1);

X = zeros(1,1);
Y = zeros(1,1);

z = int32(no);

%display(z);

num = randi([4,10],1,1);

d = int32(num);

% display('d is');
%  display(d);

D = int32(d - 1);

for l=1:z
    

    InNo   =  A(l); %randi([0,2^32],1,1);
    %display(InNo);
   
    Array = de2bi(InNo,32,2,'left-msb');
    %display(Array);
    s(l,1:32) = Array;         %s stores all the numbers as 32 bit sized arrays, in binary.
    %display(s(l,1:32));
   % display('S');
    %display(isfloat(s(1,1)));
end

numbers = int32(d);

e = idivide(z,numbers);
%display(e);

o = z-1;
h = rem(o,D);
q = D - h;

if(z> D*e+ d)
    
    Q = 1;
    
else 
    
    Q = 0;
    
end    


if(h~=0)
for l=no+1:no+q
    
    s(l,1:32) = randi([0,0],1,32);
    %display(s(l,1:32));
    
end  

end

M = e + 1*Q;

if(no==d)
    
    M = M - 1;
    
end    

Tp = zeros(1,numbers,'uint64');  % Time period 

for i = 1:numbers
    
    Buf(i,1:4) = randi([0,0],1,4);
    
end 
%display(a);


for u = 0: M 
    

for i = 1:numbers

  digital_data = s(D*u+i,1:32);
  %display(digital_data);
 

for j =1:4
    E = digital_data(8*(j-1)+1:8*(j-1)+ 8);
    %display('E');
    %display(isfloat(E(1)));
    %display(digital_data(8*(j-1)+1:8*(j-1)+ 8));
    %display(E);
    Buf(i,j) = Level_Converter(E);
    %display(a(i,j));    
end

end

w = [0,1];
%display(Buf);
%display(Tp);

t = int64(0);
 
Tp = zeros(1,numbers,'uint64'); 
for i = 1:numbers
    
    for j = 1:4
        
        w(2) = Buf(i,j);
        t    = kuramoto_x(0,w(2));
       % display(t);
        
        if(j==1)
            t = 600000000*t;
        elseif(j==2)
            t = 960000*t;
        elseif(j==3)
            t = 1500*t;
        elseif(j==4)
            t = 1*t;
        end
        
       % display(t);
        Tp(i) = Tp(i) + t;
        
    end  
    %display(Tp(i));
end

%display(Tp);


max = 1;

for j = 2:(length(Tp))
    
    if(Tp(j)>Tp(max))
        
        max = j;
        
    else    
        
    end    
    
end

%display(max);
%display('Is the largest in the set');
s(D*(u+1)+1,1:32) = s(D*u+max,1:32);

X = bi2de(s(D*u+max,1:32),2,'left-msb');
Y = X;

%s(D*(u+1)+1,1:32) = de2bi(Y,32,2,'left-msb');

% display(bi2de(s(D*u+max,1:32),2,'left-msb'));
% display(bi2de(s(D*(u+1)+1,1:32),2,'left-msb'));
% display(D*(u+1)+1);

end
%display(bi2de(s(D*(M+1)+1,1:32),2,'left-msb'));
%display(s(D*(M+1)+1,1:32));

flag = 1;
Index = 0;   % The index of the largest number
j = 1;

while((j<=z)&&(flag==1))
    
    if(s(j,1:32)==s(D*(M+1)+1,1:32))
        
        Index = j;
        flag = 0;
        
    end
    
    %display(s(j,1:32));
    j = j+1;
end    

 %display('RT');
 %display(Index);
MaxN = bi2de(s(D*(M+1)+1,1:32),2,'left-msb');  % MaxN is the largest no. in dec format.

end
% display(FN);
% display('is the largest');

    
