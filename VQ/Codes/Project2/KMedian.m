
no = randi([0,100],1,1); %36; % no represents the number of data points.

t = sqrt(no/2);
k  = int64(t) + 1; % Using rule of thumb to find the optimal number of clusters

no_a =  randi([0,20],1,1);  %4; % no_a represents the number of attribute
display(k);
display(no_a); 
ERR = zeros(1,1);

for i = 1:no_a
    Attributes(i,1:no) = zeros(1,no);  % Creating the attributes matrix
end

for j = 1:k
    
    Previous_matrix(j,1:no) = zeros(1,no);
    Group_matrix(j,1:no) = zeros(1,no); % Creating the group matrix
    Distance_matrix(j,1:no) = zeros(1,no); % Creating the distance matrix
end    


for i = 1:no_a
    
    Attributes(i,1:no) = randi([0,2^8-1],1,no);   % This fixes the maximum value of any attribute to be 2^16.
     
end  

for e = 1:no_a
    
    Centroid(e,1:k) = zeros(1,k);
    
end

display('Att');
display(Attributes);
Equal_Iterations = 0;
Iteration_number = 0;

e = randi([1,no],1,k);
%display(e);
u = double(zeros(1,1));
t = double(zeros(1,1));
y = double(zeros(1,1));

Similar_value = double(zeros(1,1));
c = double(zeros(1,1));
a = zeros(1,k,'uint32');
g = zeros(1,1);

Similar_value = zeros(1,1);
min = 0;
b = zeros(1,k);
f = zeros(1,1);

Sort_Array = zeros(1,no);
Median = zeros(1,1);

Threshold = 0.50*no;


while((Equal_Iterations<=5)&&(Similar_value<=2))
    
    if(Iteration_number==0)                           % This calculates the centroid matrix

for i = 1:k

for j = 1:no_a
    
    Centroid(j,i) =  Attributes(j,e(i));
    
end

end

    else
        
for i = 1:k
   
   u = 0;
   c = 0;
   Sort_Array = zeros(1,no);
    for j = 1:no_a
      u = 0; 
      c = 0;
      Sort_Array = zeros(1,no);
      for l = 1:no
        
        if(Group_matrix(i,l)==1)
            g = 1;
            c = c + 1;
            Sort_Array(c) = Attributes(j,l);
        else
            g = 0;
        end    
            
            %u = u + Attributes(j,l)*g;
            
      end
     

          [Median,Errors] = Sort_C(Sort_Array,c);
          Centroid(j,i) = Median;
%           display(Median);
%           display(Errors);
%           
          if(Errors>0)
              
              ERR = ERR + 1;
              display('AHHH!!ERROR!!');
              
          end    
      
    end
    
end
    end

display(Centroid);
%display(Attributes);
%display(Group_matrix);
%display(Distance_matrix);
%display(k);


 % Step 3:   Generating the group matrix

 %3.a:  Finding the minimum in each column using Min_sort3
 
for i = 1:no

    min  = Distance_x(Attributes,Centroid,i,k,no_a);
    %display(min);
   
    for j = 1:k
        
        if(j==min)
            Group_matrix(j,i) = 1;
            
        else
            Group_matrix(j,i) = 0;
            
        end
       
    end    
    
end    

display(Group_matrix);
%display(Previous_matrix);


if(Iteration_number>0)
    
    if(isequal(Group_matrix,Previous_matrix))
        
        Equal_Iterations = Equal_Iterations + 1;
        
    end
    
    f = 0;
    for i = 1:no
       
        for j = 1:k
            
           if((Group_matrix(j,i)==Previous_matrix(j,i))&&(Group_matrix(j,i)==1))
               
               f = f + 1;
               
           end
        end
        
    end   
    
end    
 
if(f>=Threshold)
    
    Similar_value = Similar_value + 1;
    
elseif(f<Threshold)
    
    Similar_value = 0;
    
end    
Iteration_number = Iteration_number + 1;
Previous_matrix = Group_matrix;
display(Iteration_number);
display(f);
display(Similar_value);
%display('Iter');
end


display(Equal_Iterations);
display(ERR);

    
    




