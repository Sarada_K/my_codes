
no = 70; % no represents the number of data points.

t = sqrt(no/2);
k  = int64(t) + 1; % Using rule of thumb to find the optimal number of clusters

no_a =  4; % no_a represents the number of attribute
display(k);
display(no_a); 

for i = 1:no_a
    Attributes(i,1:no) = zeros(1,no);  % Creating the attributes matrix
end

for j = 1:k
    
    Previous_matrix(j,1:no) = zeros(1,no);
    Group_matrix(j,1:no) = zeros(1,no); % Creating the group matrix
    Distance_matrix(j,1:no) = zeros(1,no); % Creating the distance matrix
end    


for i = 1:no_a
    
    Attributes(i,1:no) = randi([0,2^8],1,no);   % This fixes the maximum value of any attribute to be 2^16.
    
end  
%display('Att');
%display(Attributes);
Equal_Iterations = 0;
Iteration_number = 0;

e = randi([1,no],1,k);
%display(e);
u = double(zeros(1,1));
t = double(zeros(1,1));
y = double(zeros(1,1));
g = double(zeros(1,1));
c = double(zeros(1,1));
a = zeros(1,k,'uint32');
min = 0;
b = zeros(1,k);


while(Equal_Iterations<=5)
    
    if(Iteration_number==0)                           % This calculates the centroid matrix

for i = 1:k

for j = 1:no_a
    
    Centroid(j,i) =  Attributes(j,e(i));
    
end

end

    else
        
for i = 1:k
   
   u = 0;
   c = 0;
    for j = 1:no_a
      u = 0; 
      c = 0;
      for l = 1:no
        
        if(Group_matrix(i,l)==1)
            g = 1;
            c = c + 1;
        else
            g = 0;
        end    
            
            u = u + Attributes(j,l)*g;
            
      end
      
      u = u/c;
%       display(u);
%       display(c);
%       
      Centroid(j,i) = u;
      
    end
    
end
    end

%display(Centroid);
%display(Attributes);
%display(Group_matrix);
%display(Distance_matrix);
%display(k);

% Step 3:   Generating the group matrix

 %3.a:  Finding the minimum in each column using Min_sort3
 
for i = 1:no
    
    min  = Distance(Attributes,Centroid,k,no_a,i);
    %display(min);
   
    for j = 1:k
        
        if(j==min)
            Group_matrix(j,i) = 1;
            
        else
            Group_matrix(j,i) = 0;
            
        end
       
    end    
    
end    

display(Group_matrix);
%display(Previous_matrix);

if(Iteration_number>0)
    
    if(isequal(Group_matrix,Previous_matrix))
        
        Equal_Iterations = Equal_Iterations + 1;
        
    end
    
end    
    
Iteration_number = Iteration_number + 1;
Previous_matrix = Group_matrix;
display(Iteration_number);
%display('Iter');
end


display(Equal_Iterations);

    
    




