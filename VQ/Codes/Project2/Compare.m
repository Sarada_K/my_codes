
no = 30;
no_a = 4;
G = 0;

A = zeros(1,1);
B = zeros(1,1);

Similarity = zeros(1,1);

t = sqrt(no/2);
k  = int64(t)+1; % Using rule of thumb to find the optimal number of clusters

for i = 1:no_a
    Attributes(i,1:no) = zeros(1,no);  % Creating the attributes matrix
end

for i = 1:no_a
    
    Attributes(i,1:no) = randi([0,2^8-1],1,no);   % This fixes the maximum value of any attribute to be 2^16.
    
end

e = randi([1,no],1,k);

for s = 1:no_a
    
    Centroid_A(s,1:k) = zeros(1,k);
    Centroid_B(s,1:k) = zeros(1,k);
    
end

for j = 1:k
    
    Group_B(j,1:no) = zeros(1,no);
    Group_A(j,1:no) = zeros(1,no); % Creating the group matrix
    
end  


[Group_A, Centroid_A] = KMedian_fn(no,no_a,Attributes,e);
[Group_B, Centroid_B] = K_Median_fn_orig(no,no_a,Attributes,e);

Comp_Matrix = zeros(no,2);

for i = 1:no
    
    for j = 1:k
        
        y = 0;
        
        if((Group_A(j,i)==1)&&(Group_B(j,i)==1))
            
            Similarity = Similarity + 1;
           
        end
        
        y = 0;
        if(Group_A(j,i)==1)
            
            for v = 1:no_a
                
                t = Attributes(v,i) - Centroid_A(v,j);
                t = abs(t);  %% Replacement
                y = y + t;
                
            end
            
            Comp_Matrix(i,1) = y;
        end    
            y = 0;
            
         if(Group_B(j,i)==1)
             
             for v = 1:no_a
                
                t = Attributes(v,i) - Centroid_B(v,j);
                t = abs(t);  %% Replacement
                y = y + t;
                
             end
            
             Comp_Matrix(i,2) = y;
             
         end     
             
     end  
           
        
end 
    
G = 0;

for i = 1:no
    
    if(Comp_Matrix(i,1)>Comp_Matrix(i,2))
        
        G = G + 1;
    end
    
    A = A + Comp_Matrix(i,1);
    B = B + Comp_Matrix(i,2);
    
end

display(G);
    
    
            

display(Similarity);
display(Comp_Matrix);
display(A);
display(B);
display(A-B);


