%Simulation of Kuramoto for 2 oscillators

%prompt = 'Enter the number of numbers to be compared';

%input(prompt);

Error = 0;

no = randi([10,20],1,1);
z = int32(no);   % no is the number of input numbers
display(z);

num = randi([4,10],1,1);
d = int32(num);  % d is the number of oscillators
display(d);

D = int32(d - 1);

for l=1:z
    

    IntNo   =  randi([0,2^8],1,1); 
   display(IntNo);
   
    Array = de2bi(IntNo,32,2,'left-msb');
    %display(Array);
    s(l,1:32) = Array; 
    %display(s(l,1:32));
    
end

numbers = int32(d);

e = idivide(z,numbers);
%display(e);

o = z-1;
h = rem(o,D);
q = D - h;

if(z> D*e+ d)
    
    Q = 1;
    
else 
    
    Q = 0;
    
end  


if(h~=0)
for l=no+1:no+q
    
    s(l,1:32) = randi([0,0],1,32);
   % display(s(l,1:32));
    
end  

end

M = e + 1*Q;

if(no==d)
    
    M = M-1;
    
end    

% display(M);
% display(D);


for j = 1:M+1
    
    X(j,1:d) = int64(randi([0,0],1,d));  % X is the array used to store the time periods
    
    for i = 1:d
        
    Index(j,i)  =    D*(j-1) + i;   % Index is used to store index values
    
    end
    
end   

   

for u = 0: M 
    

for i = 1:numbers

  digital_data = s(D*u+i,1:32);
  %display(digital_data);
    
for j =1:4
    E = digital_data(8*(j-1)+1:8*(j-1)+ 8);
   
    a(i,j) = DAC(E);
    %display(a(i,j));    
end

end


w = [0,1];
t = int64(0);
%display(a);


for i = 1:numbers
    
    for j = 1:4
        
        w(2) = a(i,j);
        t    = kuramoto3_b(w(2),1);
        
        if(j==1)
            t = 50000000*t;
        elseif(j==2)
            t = 150000*t;
        elseif(j==3)
            t = 450*t;
        elseif(j==4)
            t = 1*t;
        end
        
        X(u+1,i) = X(u+1,i) + t;
        
    end  
 
   % display(X(u+1,1:d));
end

m = 1;

for i = 1:D

for j = 1:D
    
    if(X(u+1,j)<=X(u+1,j+1))
        
        m = X(u+1,j);
        X(u+1,j) = X(u+1,j+1);
        X(u+1,j+1) = m;
        
        m = Index(u+1,j);
        Index(u+1,j) = Index(u+1,j+1);
        Index(u+1,j+1) = m;
        
    else    
        
    end    
    
end

end

end

T = zeros(1, M+1);

% display(isfloat(T));

DIV = int64(10);
numb = int64(0);
for i=1:M+1
    
    numb = int64(X(i,1));
    numb = int64(numb);
    T(i) = double(idivide(numb,DIV));
    
end 

% display('Tfgkytliutiuyo');
% display(isfloat(T));
%display(t);
%display(Q);



%display(d*(M+1));
Tfinal = zeros(1,d*(M+1),'uint32');
Ifinal = zeros(1,d*(M+1),'uint32');
 

% GE(1,1:d*(M+1)) = randi([0,0],1,d*(M+1));
% HE(1,1:d*(M+1)) = randi([0,0],1,d*(M+1));

 %display(length(Tfinal));
 %display(length(Ifinal));
% display(G(1,1:d*(M+1)));
% display(H(1,1:d*(M+1)));

x = 1;

while( x<= d*(M+1))
    
    
    m = 1;
    
    for v= 2:M+1
        
        if(T(m)<T(v))
            
            m = v;
            
        end
        
    end 

  max = Max_sort3(T,M+1);
  
%    display(max);
%    display(m);
% % %   
% % %   display(t);
%    display(T(m));
%    display(T(max));
  
  if(T(max)~=T(m))
      
      display('ERROR!');
      Error = Error +1;
      
  end    
  
  
    
   for c = 1:M+1
        
        if(T(max)==T(c))
    
            Tfinal(x) = T(c);
            Ifinal(x) = Index(c,1);
            x = x + 1;
    
          for h=1:D
        
          X(c,h) = X(c,h+1);
          Index(c,h) = Index(c,h+1);
        
          end
    
          X(c,d) = 0;
          Index(c,d) = 0;
          
        end
        
    end    
        
        for i=1:M+1
            
            numb = int64(X(i,1));
            T(i) = idivide(numb,DIV);   %scaling the numbers by 10 so that they are lesser than 2^32, without affecting accuracy
            
         end 

        
           
        
end

% display(Tfinal);
% display(Ifinal);

for i=1:length(Ifinal)-1           %  This loop removes any repeated indices which may occur
    
    if(Ifinal(i)==Ifinal(i+1))
        
        for j = i+1:length(Ifinal)-1
            
            Ifinal(j) = Ifinal(j+1);
        end
        
    end
    
end

if(q~=D)
    
    no = no + q;
    
end

%display(no);
Ifinal2 = zeros(1,no,'uint32');     % This is the same as IFinal, but of reduced length. Hence, it will display without any repeated values.

for i = 1:no
    
    Ifinal2(i) = (Ifinal(i));
    
    
end 

% display(Ifinal);
% display(Ifinal2);
% display(q);
% display(Error);
% display(length(s));


    
    
    Value = zeros(1,z,'uint32');  % Value is the array containing the actual values of the numbers that were sorted.
   

         
         i = 1;
        while (i<=z)
           
            
            Value(i) =  bi2de(s(Ifinal2(i),1:32),2,'left-msb');
            i = i+1;
         end

display(Value);


% display('Tiout');
% display(isfloat(T));


    
    

