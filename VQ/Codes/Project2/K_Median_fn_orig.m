
function[Group_matrix,Centroid] = K_Median_fn_orig(no,no_a,Attributes,e)

%no = 36; % no represents the number of data points.
%no = 36; %randi([0,100],1,1); %36;

t = sqrt(no/2);
k  = int64(t)+1; % Using rule of thumb to find the optimal number of clusters

%no_a = 4; % randi([0,20],1,1);
%no_a =  3; % no_a represents the number of attributes

display(k);
display(no_a);

% for i = 1:no_a
%     Attributes(i,1:no) = zeros(1,no);  % Creating the attributes matrix
% end

for j = 1:k
    
    Previous_matrix(j,1:no) = zeros(1,no);
    Group_matrix(j,1:no) = zeros(1,no); % Creating the group matrix
    Distance_matrix(j,1:no) = zeros(1,no); % Creating the distance matrix
end    


% for i = 1:no_a
%     
%     Attributes(i,1:no) = randi([0,2^16],1,no);   % This fixes the maximum value of any attribute to be 2^16.
%     
% end  

Equal_Iterations = 0;
Iteration_number = 0;

%e = randi([1,no],1,k);
display(e);

u = double(zeros(1,1));
t = double(zeros(1,1));
y = double(zeros(1,1));
g = double(zeros(1,1));

c = zeros(1,1);
a = zeros(1,k,'uint32');
f = zeros(1,1);
Index = zeros(1,1);

Threshold = 0.75*no;

Similar_value = zeros(1,1);
Sort_Array = [];
Median = double(zeros(1,1));
d = zeros(1,1,'uint32');

%min = 0;
b = zeros(1,k);


while((Equal_Iterations<=5)&&(Similar_value<=2))
    
    if(Iteration_number==0)                           % This calculates the centroid matrix

for i = 1:k

for j = 1:no_a
    
    Centroid(j,i) =  Attributes(j,e(i));
    
end

end

    else
        
for i = 1:k
   
    u = 0;
    c = 0;
    Sort_Array = [];%zeros(1,no);
    
    for j = 1:no_a
      u = 0; 
      c = 0;
      Sort_Array = []; %zeros(1,no);
      
      for l = 1:no
        
        if(Group_matrix(i,l)==1)
            g = 1;
            c = c + 1;
            %Sort_Array(c) = Attributes(j,l);
            Sort_Array = cat(2,Sort_Array,Attributes(j,l));
        else    
            g = 0;
        end    
            
           % u = u + Attributes(j,l)*g;
           Sort_Array = sort(Sort_Array);
           Remainder =  rem(c,2);
           e = int32(2);
           D = int32(c);
           d = idivide(D,e);
           
           if(Remainder==1)
               
               Median = Sort_Array(d+1);
               
           else
               
               if(c~=0)
                   Median  = 0.5*(Sort_Array(d) + Sort_Array(d+1));
               else
                   Median = 0;
               end    
               
           end    
           
           
       end
     
      Centroid(j,i) = Median;
      
    end
    
end
    end

display(Centroid);
display(Attributes);
display(Group_matrix);
display(Distance_matrix);
display(k);


for r = 1:k              % This loop calculates the Distance matrix for each step
    
    y = 0;
    for i = 1:no
        y = 0;
        
        for j = 1:no_a
            
            t = Attributes(j,i) - Centroid(j,r);
            t = abs(t);  %% Replacement
            y = y + t;
            
        end
        
        Distance_matrix(r,i) = sqrt(y);
        
    end
    
end    

display(Distance_matrix);

 % Step 3:   Generating the group matrix

 %3.a:  Finding the minimum in each column using Min_sort3
 
for i = 1:no
    
    for j = 1:k
        
        a(j) = Distance_matrix(j,i);
        b(j) = a(j);
        
    end
    
    %display(isfloat(b));
    %min  = Min_sort(a);
    [Min,Index] = min(a);                %First replacement
   % display(Index); 
   
    for j = 1:k
        
        if(j==Index)
            Group_matrix(j,i) = 1;
            
        else
            Group_matrix(j,i) = 0;
            
        end
       
    end    
    
end    

display(Group_matrix);
display(Previous_matrix);

if(Iteration_number>0)
    
    if(isequal(Group_matrix,Previous_matrix))
        
        Equal_Iterations = Equal_Iterations + 1;
        
    end
    
    f = 0;
    for i = 1:no
       
        for j = 1:k
            
           if((Group_matrix(j,i)==Previous_matrix(j,i))&&(Group_matrix(j,i)==1))
               
               f = f + 1;
               
           end
        end
        
    end   
    
end    
 
if(f>=Threshold)
    
    Similar_value = Similar_value + 1;
    
elseif(f<Threshold)
    
    Similar_value = 0;
    
end    
Iteration_number = Iteration_number + 1;
Previous_matrix = Group_matrix;
display(Iteration_number);
display(f);
display(Similar_value);

end


display(Equal_Iterations);

end

    
    




